<?php
/**
 * Created by PhpStorm.
 * User: herizo
 * Date: 11/12/18
 * Time: 1:22 PM
 */

namespace Tests\Repository;


use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PostRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSearchPostByTitle()
    {
        $post = $this->entityManager
            ->getRepository(Post::class)
            ->searchPostByTitle('teset');

        $this->assertCount(
            1,
            $post
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }
}